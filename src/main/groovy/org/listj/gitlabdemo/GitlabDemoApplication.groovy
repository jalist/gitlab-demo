package org.listj.gitlabdemo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class GitlabDemoApplication {

	static void main(String[] args) {
		SpringApplication.run(GitlabDemoApplication, args)
	}

}
